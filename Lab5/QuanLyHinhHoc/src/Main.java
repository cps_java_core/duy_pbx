import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    createObj("3");
    createObj("3 4");
    createObj("3 3");
    createObj("3 4 5");

    //thuaDat("1 2 4");
    thuaDat("1 3 2 4");
  }

  static int[] getInput(String str) {
    String[] wordArray = str.trim().split("\\s+");

    int[] numArray = new int[wordArray.length];

    for (int i = 0; i < wordArray.length; i++) {
      numArray[i] = Integer.parseInt(wordArray[i]);
    }

    return numArray;
  }

  static void createObj(String str) {
    String[] wordArray = str.trim().split("\\s+");

    int[] numArray = new int[wordArray.length];

    for (int i = 0; i < wordArray.length; i++) {
      numArray[i] = Integer.parseInt(wordArray[i]);
    }

    switch (numArray.length) {
    case 1:
      System.out.println("Tao hinh tron");
      Circle c = new Circle(numArray[0]);
      System.out.println("Dien tich hinh tron: " + c.getArea());
      System.out.println("Chu vi hinh tron: " + c.getPerimeter());
      break;
    case 2:
      if (numArray[0] == numArray[1]) {
        System.out.println("Tao hinh vuong");
        Square s = new Square(numArray[0]);
        System.out.println("Dien tich hinh vuong: " + s.getArea());
        System.out.println("Chu vi hinh vuong: " + s.getPerimeter());
      } else {
        System.out.println("Tao hinh chu nhat");
        Rectangle r = new Rectangle(numArray[0], numArray[1]);
        System.out.println("Dien tich hinh chu nhat: " + r.getArea());
        System.out.println("Chu vi hinh chu nhat: " + r.getPerimeter());
      }
      break;
    case 3:
      System.out.println("Tao hinh tam giac");
      Triangle t = new Triangle(numArray[0], numArray[1], numArray[2]);
      System.out.println("Dien tich hinh tam giac: " + t.getArea());
      System.out.println("Chu vi hinh tam giac: " + t.getPerimeter());
      break;
    default:
      System.out.println("Khong tao duoc");
    }
    System.out.println("============");
  }

  static void createObj(int[] numArray) {
    switch (numArray.length) {
    case 1:
      System.out.println("Tao hinh tron");
      Circle c = new Circle(numArray[0]);
      System.out.println("Dien tich hinh tron: " + c.getArea());
      System.out.println("Chu vi hinh tron: " + c.getPerimeter());
      break;
    case 2:
      if (numArray[0] == numArray[1]) {
        System.out.println("Tao hinh vuong");
        Square s = new Square(numArray[0]);
        System.out.println("Dien tich hinh vuong: " + s.getArea());
        System.out.println("Chu vi hinh vuong: " + s.getPerimeter());
      } else {
        System.out.println("Tao hinh chu nhat");
        Rectangle r = new Rectangle(numArray[0], numArray[1]);
        System.out.println("Dien tich hinh chu nhat: " + r.getArea());
        System.out.println("Chu vi hinh chu nhat: " + r.getPerimeter());
      }
      break;
    case 3:
      System.out.println("Tao hinh tam giac");
      Triangle t = new Triangle(numArray[0], numArray[1], numArray[2]);
      System.out.println("Dien tich hinh tam giac: " + t.getArea());
      System.out.println("Chu vi hinh tam giac: " + t.getPerimeter());
      break;
    default:
      System.out.println("Khong tao duoc");
    }
    System.out.println("============");
  }

  static void thuaDat(String str) {
    String[] wordArray = str.trim().split("\\s+");

    int[] numArray = new int[wordArray.length];

    for (int i = 0; i < wordArray.length; i++) {
      numArray[i] = Integer.parseInt(wordArray[i]);
    }

    if (numArray.length != 4){
      System.out.println("Input ko hop le");
      return;
    }
    int a = numArray[0];
    int b = numArray[1];
    int c = numArray[2];
    int d = numArray[3];

    Rectangle r = new Rectangle(a, b);
    Triangle t = new Triangle(b, c, d);
    Square s = new Square(c);
    Circle circle = new Circle(a);

    double rs=r.getArea() + t.getArea() + s.getArea() + circle.getArea() / 2;

    System.out.println("Dien tich thua that la "+ rs); 
  }
}
