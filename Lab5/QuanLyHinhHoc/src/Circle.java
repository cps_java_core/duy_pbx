class Circle implements IShape{
    private int radius;
    
    public Circle(int radius){
        this.radius=radius;
    }
    
    @Override
    public double getArea(){
        return radius*radius*3.14;
    }
    
    @Override
    public double getPerimeter(){
        return radius*3.14*2;
    }
}