public class Triangle implements IShape {
  private int a;
  private int b;
  private int c;

  public Triangle(int a, int b, int c){
        this.a=a;
        this.b=b;
        this.c=c;
    }

  @Override
  public double getArea() {
    double p=this.getPerimeter();
    return Math.sqrt(p * (p - a) * (p - b) * (p - c));
  }

  @Override
  public double getPerimeter() {
    return a+b+c;
  }
}
