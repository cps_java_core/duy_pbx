public class Square implements IShape{
    private int length;
    
    public Square(int length){
        this.length=length;
    }
    
    @Override
    public double getArea(){
        return length*length;
    }
    
    @Override
    public double getPerimeter(){
        return 4*(length);
    }
}
